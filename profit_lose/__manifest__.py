{
    "name": "Profit and Lose",
    "version": "11.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
        "ecua_autorizaciones_sri",
        "ecua_documentos_electronicos",
        "ecua_documentos_sri",
    ],
    "data": [
        # security
        # data
        # reports
        "reports/account_invoice_profit.xml",
        # views
        "views/account_invoice_profit.xml",
    ],
}
