from odoo import fields, models


class AccountInvoiceProfit(models.TransientModel):
    _name = "account.invoice.profit"
    _description = "Account Invoice Profit"

    start = fields.Date(
        required=True,
        string="Start date",
        default=fields.Datetime.now,
    )
    end = fields.Date(
        required=True,
        string="End date",
        default=fields.Datetime.now,
    )
    printer_id = fields.Many2one(
        comodel_name="sri.printer.point",
        required=True,

    )



    def generate(self):
        report = self.env.ref("profit_lose.profit_lose_report")
        return report.report_action(self)

    def get_lines(self):
        lines = self.env["account.invoice.line"].search(
            [
                ("invoice_id.type", "=", "out_invoice"),
                ("invoice_id.state", "=", "paid"),
                ("invoice_id.date_invoice", ">=", self.start),
                ("invoice_id.date_invoice", "<=", self.end),
                ("invoice_id.printer_id", "=", self.printer_id.id),
            ]
        )

        return lines
